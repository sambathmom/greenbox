<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\QuestionRepository;
use App\Entities\Question;
use App\Validators\QuestionValidator;

/**
 * Class QuestionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class QuestionRepositoryEloquent extends BaseRepository implements QuestionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Question::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return QuestionValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Get questions by user Id
     * 
     * @param $userId
     * 
     * @return Collection
     */
    public function getQuestionsByUserId($userId) 
    {
        $questions = Question::where('user_id', $userId)->orderBy('updated_at', 'DESC')->paginate(10);

        return $questions;
    }

    /**
     * Get all question by where in answer id
     * 
     * @param array $answersId
     * 
     * 
     * @return Collection
     */
    public function getQuestoinsByWhereInQuestonsId($questionsId)
    {
        $questions = Question::whereIn('id', $questionsId)->orderBy('updated_at', 'DESC')->paginate(10);

        return $questions;
    }

    public function getQuestionsByCategoryId($categoryId)
    {
        $questions = Question::where('cate_id', $categoryId)->orderBy('updated_at', 'DESC')->paginate(10);
        
        return $questions;
    }

    public function getQuestoinsByQuestionStr($searchStr)
    {
        $questions = Question::where('title', 'like', '%' .  $searchStr . '%')->get();
        
        return $questions;
    }

    public function getQuestionsBySearchStrAndUserId($searchStr,$userId) 
    {
        $questions = Question::where('user_id', $userId)->where('title', 'like', '%' .  $searchStr . '%')
                    ->get();

        return $questions;
    }

    public function getQuestoinsByWhereInQuestonsIdAndSearchStr($questionsId, $searchStr)
    {
        $questions = Question::whereIn('id', $questionsId)
                    ->where('title', 'like', '%' .  $searchStr . '%')
                    ->get();
        
        return $questions;
    }
    
}
