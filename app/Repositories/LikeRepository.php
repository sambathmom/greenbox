<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LikeRepository.
 *
 * @package namespace App\Repositories\Entities;
 */
interface LikeRepository extends RepositoryInterface
{
    //
}
