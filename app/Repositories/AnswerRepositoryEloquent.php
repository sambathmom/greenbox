<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AnswerRepository;
use App\Entities\Answer;
use App\Validators\AnswerValidator;

/**
 * Class AnswerRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AnswerRepositoryEloquent extends BaseRepository implements AnswerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Answer::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AnswerValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * Get all answers by question id
     * 
     * @param int $questionId
     * 
     * @return Collection
     */
    public function getAnswersByQuestionId($questionId)
    {
        $answers = Answer::where('question_id', $questionId)->get();

        return $answers;
    }

    /**
     * Get all answers by user id
     * 
     * @param int $userId
     * 
     * @return Collection
     */
    public function getAnswersByUserId($userId) 
    {
        $answers = Answer::where('user_id', $userId)->get();

        return $answers;
    }

    /**
     * Find answer by question id and user id
     * 
     * @param int $userId
     * @param int $questionId
     * 
     * @return Answer
     */
    public function findAnserByUserIdAndQuestionId($userId, $questionId)
    {
        $answer = Answer::where([['user_id', $userId], ['question_id', $questionId]])->first();

        return $answer;
    }
}
