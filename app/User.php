<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Entities;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'name', 
        'email', 
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Entites\Role');
    }

    public function questions()
    {
        return $this->hasMany('App\Entities\Question');
    }

    public function comments()
    {
        return $this->hasMany('App\Entities\Comment');
    }

    public function likes()
    {
        return $this->hasMany('App\Entities\Like');
    }
}
