<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\QuestionCreateRequest;
use App\Http\Requests\QuestionUpdateRequest;
use App\Repositories\QuestionRepositoryEloquent;
use App\Validators\QuestionValidator;
use App\Entities\Question;
use Illuminate\Support\Facades\Auth;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\CommentRepositoryEloquent;
use App\Repositories\AnswerRepositoryEloquent;
use Illuminate\Validation\Rule;

/**
 * Class QuestionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class QuestionsController extends Controller
{
    /**
     * @var QuestionRepositoryEloquent
     */
    protected $repository;

    /**
     * @var QuestionValidator
     */
    protected $validator;

    /**
     * @var CategoryRepositoryEloquent
     */
    protected $categoryRepository;

    /**
     * @var AnswerRepositoryEloquent
     */
    protected $answerRepository;

    /**
     * @var CommentRepositoryEloquent
     */
    protected $commentRepository;

    
    /**
     * QuestionsController constructor.
     *
     * @param QuestionRepositoryEloquent $repository
     * @param QuestionValidator $validator
     */
    public function __construct(QuestionRepositoryEloquent $repository, QuestionValidator $validator, 
        CategoryRepositoryEloquent $categoryRepository, AnswerRepositoryEloquent $answerRepository,
        CommentRepositoryEloquent $commentRepository)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->categoryRepository = $categoryRepository;
        $this->answerRepository = $answerRepository;
        $this->commentRepository = $commentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = auth()->user()->id;
        $categories = $this->categoryRepository->all();
        $questions = $this->repository->getQuestionsByUserId($userId);

        $countQuestoins = $questions->count();
        $countAnswers = $this->answerRepository->getAnswersByUserId($userId)->count();


        return view('questions.index', compact('questions', 'categories', 'countQuestoins', 'countAnswers'));
    }


    /**
     * Create question
     * 
     */
    public function create()
    {
        $categories = $this->categoryRepository->all();

        return view('questions.create-question', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  QuestionCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:questions',
        ]);

        $question = new Question; 
        $question->cate_id = $request->cate_id;
        $question->title = $request->title;
        $question->description = $request->description;
        $question->user_id = Auth::user()->id;

        $destination = 'img_upload';
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = $image->getClientOriginalName();
            $image->move($destination, $filename);
            $question->image = $filename;
        }

        
        $question->save();

        return redirect()->route('questions.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($questionId)
    {
        $question = $this->repository->find($questionId);
        return view('questions.detail', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($questionId)
    {
        $question = $this->repository->find($questionId);
        $categories = $this->categoryRepository->all();

        return view('questions.edit', compact('question', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     *
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:questions,title,'. $request->id
        ]);


        $question = Question::findOrFail($request->id); 
        $question->cate_id = $request->cate_id;
        $question->title = $request->title;
        $question->description = $request->description;
        $question->user_id = Auth::user()->id;

        $destination = 'img_upload';
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = $image->getClientOriginalName();
            $image->move($destination, $filename);
            $question->image = $filename;
        }

        $question->save();
        
       // $question = $this->repository->update($request->all(), $request->id);

        return  redirect()->route('questions.index');
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $questionId = $request->id;
        $answers = $this->answerRepository->getAnswersByQuestionId($questionId);

        if ($answers) {
            foreach($answers as $answer) {
                $comments = $this->commentRepository->getCommentsByAnswerId($answer->id);
                
                if ($comments) {
                    foreach($comments as $comment) {
                        $this->commentRepository->delete($comment->id);
                    }
                }
                $this->answerRepository->delete($answer->id);
            }
        }
    
        $this->repository->delete($questionId);

        return redirect()->back()->with('message', 'Question deleted.');
    }

    /**
     * Get all qustions were answered by auth user
     * 
     */
    public function qustoinsAnswered()
    {
        $userId = auth()->user()->id;
        $answers = $this->answerRepository->getAnswersByUserId($userId);

        $arrayQuestionsId = [];
        foreach ($answers as $answer) {
            if (!in_array($answer->question_id, $arrayQuestionsId)) {
            $arrayQuestionsId[] = $answer->question_id;            
            }
        }

        $questions = $this->repository->getQuestoinsByWhereInQuestonsId($arrayQuestionsId);
        $categories = $this->categoryRepository->all();

        $countAnswers = $answers->count();
        $countQuestoins = $this->repository->getQuestionsByUserId($userId)->count();

        return view('questions.questions-answered', compact('questions', 'categories', 'countAnswers', 'countQuestoins'));
    }

    public function getQuestionsByCategoryId($categoryId)
    {
        $userId = auth()->user()->id;
        $categories = $this->categoryRepository->all();
        $questions = $this->repository->getQuestionsByCategoryId($userId);

        $countQuestoins = $questions->count();
        $countAnswers = $this->answerRepository->getAnswersByUserId($userId)->count();

        return view('questions.index', compact('questions', 'categories', 'countQuestoins', 'countAnswers'));
    }

    public function getQuestionSearch(Request $request)
    {
        $searchStr = $request->search;
        $userId = auth()->user()->id;
        $categories = $this->categoryRepository->all();
        $questions = $this->repository->getQuestionsBySearchStrAndUserId($searchStr,$userId);

        $countQuestoins = $questions->count();
        $countAnswers = $this->answerRepository->getAnswersByUserId($userId)->count();


        return view('questions.index', compact('questions', 'categories', 'countQuestoins', 'countAnswers', 'searchStr'));
    }

    public function getAnswerQuestoinsSearch(Request $request)
    {
        $searchStr = $request->search;
        $userId = auth()->user()->id;
        $answers = $this->answerRepository->getAnswersByUserId($userId);
        
        $arrayQuestionsId = [];
        foreach ($answers as $answer) {
            if (!in_array($answer->question_id, $arrayQuestionsId)) {
                $arrayQuestionsId[] = $answer->question_id;            
            }
        }

        $questions = $this->repository->getQuestoinsByWhereInQuestonsIdAndSearchStr($arrayQuestionsId,$searchStr);
        $categories = $this->categoryRepository->all();

        $countAnswers = $answers->count();
        $countQuestoins = $this->repository->getQuestionsByUserId($userId)->count();

        return view('questions.questions-answered', compact('questions', 'categories', 'countAnswers', 'countQuestoins', 'searchStr'));
    }
}
