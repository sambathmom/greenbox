<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function editProfile($userId) 
    {

        if(auth()->user()->role_id == 1) {
            return view('admins.users.admin-edit-profile');
        }
        return view('users.edit-profile');        
    }

    public function update(Request $request)
    {
        $user = User::findOrFail(auth()->user()->id);
        $destination = 'images_profile';
        
        if ($request->hasFile('image')) {
            $icon = $request->file('image');
            $filename = $icon->getClientOriginalName();
            $icon->move($destination, $filename);
            $user->image = $filename;        
        }

        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password != '') {
            $user->password = $request->password;
        }

        $user->save();

        if (auth()->user()->role_id == 1) {
            return redirect()->route('admins.users.index');
        
        }
        return redirect()->route('questions.index');
        
    }
}
