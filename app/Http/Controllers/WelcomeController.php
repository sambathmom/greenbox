<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\QuestionRepositoryEloquent;
use App\Repositories\CategoryRepositoryEloquent;

class WelcomeController extends Controller
{
     /**
     * @var QuestionRepositoryEloquent
     */
    protected $questionRepository;
    
    
    /**
     * @var CategoryRepositoryEloquent
     */
    protected $categoryRepository;

    /**
    * QuestionsController constructor.
    *
    * @param QuestionRepositoryEloquent $repository
    */
   public function __construct(QuestionRepositoryEloquent $repository, CategoryRepositoryEloquent $categoryRepository)
   {
       $this->repository = $repository;
       $this->categoryRepository = $categoryRepository;
   }
    
    public function welcome() 
    {
        $questions = $this->repository->orderBy('updated_at', 'DESC')->paginate(10);
        $categories = $this->categoryRepository->all();
        return view('welcome', compact('questions', 'categories'));
    }

    public function questionDetail($questionId)
    {
        $question = $this->repository->find($questionId);
        $categories = $this->categoryRepository->all();
        return view('questions.detail', compact('question', 'categories'));
    }

    public function getQuestionsByCategoryId($categoryId)
    {
        $categories = $this->categoryRepository->all();
        $questions = $this->repository->getQuestionsByCategoryId($categoryId);

        return view('question-type', compact('questions', 'categories'));
    }

    public function getQuestoinSearch(Request $request)
    {
        $searchSt = $request->search;
        
        $questions = $this->repository->getQuestoinsByQuestionStr($searchSt);
        $categories = $this->categoryRepository->all();

        return view('welcome', compact('questions', 'categories', 'searchSt'));
    }

    public function admin(Request $req){
        return view(‘middleware’)->withMessage(“Admin”);
    }
}
