<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\AnswerCreateRequest;
use App\Http\Requests\AnswerUpdateRequest;
use App\Repositories\AnswerRepositoryEloquent;
use App\Validators\AnswerValidator;
use App\Repositories\QuestionRepositoryEloquent;
use App\Entities\Answer;
use Illuminate\Support\Facades\Auth;
use App\Repositories\CommentRepositoryEloquent;


/**
 * Class AnswersController.
 *
 * @package namespace App\Http\Controllers;
 */
class AnswersController extends Controller
{
    /**
     * @var AnswerRepositoryEloquent
     */
    protected $repository;

    /**
     * @var AnswerValidator
     */
    protected $validator;

    /**
     * @var QuestionRepositoryEloquent
     */
    protected $questionRepository;

    /**
     * @var CommentRepositoryEloquent
     */
    protected $commentRepository;

    /**
     * AnswersController constructor.
     *
     * @param AnswerRepository $repository
     * @param QuestionRepositoryEloquent $questionRepository
     * @param AnswerValidator $validator
     */
    public function __construct(AnswerRepositoryEloquent $repository, AnswerValidator $validator, 
    QuestionRepositoryEloquent $questionRepository, CommentRepositoryEloquent $commentRepository)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->questionRepository = $questionRepository;
        $this->commentRepository = $commentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $answers = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $answers,
            ]);
        }

        return view('answers.index', compact('answers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AnswerCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'answer' => 'required'
        ]);

        $answer = new Answer; 
        $answer->question_id = $request->question_id;
        $answer->answer = $request->answer;
        $answer->user_id = Auth::user()->id;

        $answer->save();

        return redirect()->back();
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $answer = $this->repository->find($id);

        return view('answers.edit', compact('answer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     *
     */
    public function update(Request $request)
    {

        $this->validate($request, [
            'answer' => 'required'
        ]);

        $this->repository->update($request->all(), $request->id);
        
        return  redirect()->route('answers.question.detail', $request->question_id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $answerId = $request->id;
        $comments = $this->commentRepository->getCommentsByAnswerId($answerId);
        
        if ($comments) {
            foreach($comments as $comment) {
                $this->commentRepository->delete($comment->id);
            }
        }

        $this->repository->delete($answerId);

        return redirect()->back()->with('message', 'Answer deleted.');
    }

    public function answerQuestionDetail($questionId)
    {
        $userId = auth()->user()->id;
        $question = $this->questionRepository->find($questionId);
        $countQuestoins = $this->questionRepository->getQuestionsByUserId($userId)->count();;
        $countAnswers = $this->repository->getAnswersByUserId($userId)->count();

        $answer = $this->repository->findAnserByUserIdAndQuestionId($userId, $questionId);
        
        return view('answers.my-asnwer', compact('question', 'countQuestoins', 'countAnswers', 'answer'));
    }
}
