<?php

namespace App\Http\Controllers\Admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class AdminController extends Controller
{
    public function index() 
    {
        return view('admins.index');
    }

    public function getUsers() 
    {
        $users = User::all();
       // dd($users);

       return view('admins.users.index', compact('users'));
    }
}
