<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\LikeCreateRequest;
use App\Http\Requests\LikeUpdateRequest;
use App\Repositories\LikeRepositoryEloquent;
use App\Entities\Like;

/**
 * Class LikesController.
 *
 * @package namespace App\Http\Controllers\Entities;
 */
class LikesController extends Controller
{
    /**
     * @var LikeRepository
     */
    protected $repository;


    /**
     * LikesController constructor.
     *
     * @param LikeRepositoryEloquent $repository
     * @param LikeValidator $validator
     */
    public function __construct(LikeRepositoryEloquent $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $likes = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $likes,
            ]);
        }

        return view('likes.index', compact('likes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LikeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $userId = auth()->user()->id;
        $answerId = $request->answer_id;

        $like = new Like;

        $like->user_id = $userId;
        $like->answer_id = $answerId;
        $like->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $like = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $like,
            ]);
        }

        return view('likes.show', compact('like'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $like = $this->repository->find($id);

        return view('likes.edit', compact('like'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  LikeUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(LikeUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $like = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Like updated.',
                'data'    => $like->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Like deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Like deleted.');
    }
}
