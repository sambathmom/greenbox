<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Like.
 *
 * @package namespace App\Entities\Entities;
 */
class Like extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'likes_answer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'answer_id'
    ];

    public function answer()
    {
        return $this->belongsTo('App\Entities\Answer');
    }

    public function user() 
    {
        return $this->belongTo('App\User');
    }
}
