<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Question.
 *
 * @package namespace App\Entities;
 */
class Question extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'cate_id',
        'title',
        'description',
        'like',
        'image'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Entities\Category', 'cate_id');
    }

    public function answers()
    {
        return $this->hasMany('App\Entities\Answer');
    }

    public function comments()
    {
        return $this->hasMany('App\Entities\Comment');
    }

}
