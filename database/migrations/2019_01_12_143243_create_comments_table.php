<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCommentsTable.
 */
class CreateCommentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('user_id');
			$table->unsignedInteger('answer_id');
			$table->unsignedInteger('question_id');
			$table->text('comment');
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('answer_id')->references('id')->on('answers');
			$table->foreign('question_id')->references('id')->on('questions');
            
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}
}
