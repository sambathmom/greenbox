@extends('layouts.user')



@section('content')
<div class="row mt-5">
    <div class="col-md-1 col-12">&nbsp;</div>
    <div class="col-md-7 col-12 ml-3  ml-md-0">

        <!-- Start Dropdown -->
        <div class="dropdown show">
            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Top Question
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="#">Top Question</a>
                <a class="dropdown-item" href="#">Top Vote</a>

            </div>
        </div>
        <!-- Start Dropdown -->

        <div>
            <!-- <img class="ml-4" src="../Resources/profilepic.jpg" style="width:38px;height:38px;border-radius:50%"
                alt=""> -->
            <!-- start Card -->
            <div class="mycard card mt-5" style="width: 100%">
                <div class="card-body">
                    <div class="float-right border bg-light rounded" style="position: absolute; top:-18px;border-radius: 8px !important; ">
                        <img src="../Resources/swift.svg" class="mr-0  col-12" style="width: 35px;padding-left: 0px;padding-right: 0px;"
                            alt="">
                        <label for="" class="pr-2 ">Swift</label>
                    </div>
                    <div class="row">
                        <div class="mt-4 userArea col-2">
                            <img class="qprofile" src="../Resources/profilepic.jpg" alt="">
                            <p class="qname text-xs-center">VattanacSim</p>
                        </div>

                        <div class="col-sm-10 col-md-10 col-12">
                            <div>
                                <h5 class="card-title">React component table is being rendered multiple times or
                                    duplicated after updating state</h5>
                                <div class="">
                                    <img class="test" src="../Resources/date.svg" style="width:15px;height:15px;"
                                        alt="">
                                    <h6 class="ml-4 card-subtitle mb-2 text-muted">4hours ago</h6>
                                </div>

                                <p class="card-text">I'm updating state with changes in props by replacing the old
                                    state. In render, the data is either being duplicated or rendering multiple
                                    times
                                    from the state after it is reset with new props. It is happening from after the
                                    first render.</p>
                            </div>
                            <div class="d-flex justify-content-around mt-2">
                                <span><img src="../Resources/answer.svg" style="width:25px;height:25px" alt="">38
                                    answers</span>
                                <span><img src="../Resources/comment.svg" style="width:25px;height:25px" alt="">38
                                    comments</span>
                                <span><img src="../Resources/vote.svg" style="width:25px;height:25px" alt="">38
                                    likes</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Card -->
        </div>
    </div>

    <!-- Start Tage -->
    <div class="col-md-4 col-12 ">
        <div class="mt-5 ">
            <label for="">Tages</label>
            <div class="border-bottom mr-5 col-12" style="width:70%">
                <img src="../Resources/swift.svg" class="col-12" style=" width: 50px;" alt="">Switf
            </div>
            <div class="border-bottom mr-5 col-12" style="width:70%">
                <img src="../Resources/swift.svg" class="col-12" style=" width: 50px;" alt="">Switf
            </div>

        </div>
    </div>
    <!-- End Tage -->
</div>
@endsection