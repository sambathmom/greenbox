@extends('layouts.admin')

@section('title')
    Categories
@endsection

@section('content')
    <div class="col-md-12">
        <div>
            <a href="{{route('admins.categories.create')}}" style = "text-align: center;font-weight: bold;font-family: 'Apple Chancery';font-size:20px;"> Create Categories </a>
        </div>
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Title</th>
                    <th scope="col">Image</th> 
                    <th scope="col">Created at</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $key=>$category)
                <tr class = "center">
                    <th scope="row">{{$key+1}}</th>
                    <td>{{$category->name}}</td>
                    <td>
                        <img src="{{asset('img_upload/' . $category->icon . '')}}" style= "height:30px;">
                    </td>
                    <td>{{$category->created_at}}</td>
                    <td><a href="{{route('admins.categories.edit', $category->id)}}">Edit</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- The Modal -->
<div class="modal" id="deleteCategoryModal">
    <div class="modal-dialog">
        <form method="post" action="{{route('admins.categories.destroy')}}">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Comfirmation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            
                <!-- Modal body -->
                <div class="modal-body">
                    @csrf
                    <input type="hidden" id="deleteCategory" name="id" >
                    Are you sure want to delete this category?
                </div>
            
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Delete"/>                    
                </div>
        
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('.delete-category').on('click', function() {
            alert('hi');
            $questionId = $(this).data('id');
            $('#deleteCategory').val($questionId);
            $('#deleteCategoryModal').modal('show');
        });
        
    });

</script>
@endsection