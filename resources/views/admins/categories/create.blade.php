@extends('layouts.admin')

@section('content')
    <div class="col-md-12" >
        <h1 id = "title"> Create Category </h1>
        <form action="{{route('admins.categories.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            
            <input type="hidden" class="form-control" name="id">            
            <div class="form-group" style = "margin-right:2%;">
                <label for="pwd">Name:</label>
                <input type="text" class="form-control" name="name" id="cat_name">
                <br>
                <input type="submit" class="btn btn-primary" value="Save"/>
            </div>
            
            <div class="form-group">
                <label for="pwd">Icon:</label>
            
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile2" id="circle">
                        <input type="file"  name="icon" id="file" 
                                title="" accept="image/gif, image/jpeg, image/png" class="" > 
                    </div>
            
            </div>
        </form>
    </div>  
    
    <div class="col-md-1"></div>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#file").change(function(e) {
                var input = document.getElementById("file");
                var fReader = new FileReader();
                fReader.readAsDataURL(input.files[0]);
                fReader.onloadend = function(event){
                    $("#file").parent().css( "background-image", "url("+event.target.result+")"  );
                }
            });

        });     
    </script>

    <style>
        .content1{
            border: 1px solid gray;
            margin-top: 2%;
            padding: 15px;
            padding-right:0px;
        }
        #title{
            text-align: center;
            font-weight: bold;
            font-family: "Apple Chancery";
            font-size:40px;
            margin-bottom:3%
        }
        label{
            font-weight: 300;
            font-size:20px;
        }
        input[type='text']{
            font-weight: 300;
            font-size:20px;
            padding-bottom:10px;
        }
        .form-group{
            width:48%;
            float:left;
        }
        input[type='submit']{
            width:100%;

        }
        .profile2{
            opacity:1;
            height:100px;
            width:100%;
            //background-image: url('{{asset("images_profile/" . auth()->user()->image . "")}}');
            background-color: #cccccc; /* Used if the image is unavailable */
            background-position: center; /* Center the image */
            background-repeat: no-repeat; /* Do not repeat the image */
            background-size: contain;
            border: 1px solid gray;
            padding:0px;
        }
        #file{
            height:100%;
            width:100%;
            cursor: pointer;
            opacity:0;
        }
    </style>

@endsection