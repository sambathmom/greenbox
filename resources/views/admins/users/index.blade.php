@extends('layouts.admin')

@section('title')
    User
@endsection

@section('content')
    <div class="col-md-12">
    
        <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th>Image</th>
                <th>Created at </th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $key=>$user)
                <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    @if($user->image != null || $user->image != "")
                    <img src="{{asset('images_profile/' . $user->image . '')}}" style= "height:30px;">
                    @else
                    <img src="{{asset('resources/profilepic.jpg')}}" style= "height:30px;">                    
                    @endif
                </td>
                <td>{{$user->created_at}} </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection