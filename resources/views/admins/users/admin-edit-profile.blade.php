@extends('layouts.admin')

@section('title')
    Categories
@endsection

@section('content')
    <div class="col-md-12 col-lg-12 col-12 ml-5  ml-md-0">
        <!-- Start filter -->
        <div class="row offset-md-2 col-lg-12" id="form_container">
            <div class="col-lg-1"></div>
            <form method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data" class="col-lg-10">
                @csrf

                    <div id="cover" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fl" ></div> 
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fl profile">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile1">
                            @if(auth()->user()->image != null || auth()->user()->image != '')
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile2" id="circle" style='background-image: url({{asset("images_profile/" . auth()->user()->image . "")}})'>
                                    <input type="file" id="file" style="width:220px;height:230px;" 
                                            title="" accept="image/gif, image/jpeg, image/png" class="form-control" name="image"> 
                                </div>
                            @else
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 profile2" id="circle" style='background-image: url({{asset("resources/profilepic.jpg")}})'>
                                    <input type="file" id="file" style="width:220px;height:230px;" 
                                            title="" accept="image/gif, image/jpeg, image/png" class="form-control" name="image"> 
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fl"></div> 
                    </div>

                    <div class="form-group row">
                        <label for="info " class="col-md-12 col-form-label mt-5 " style="font-weight: bold;font-size: 20px;">Edit Profile </label>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-12 col-form-label">{{ __('Name') }}</label>
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control" name="name" required value="{{auth()->user()->name}}" >

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control" name="email" required value="{{auth()->user()->email}}" >

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="password" class="col-md-12 col-form-label" name="password">{{ __('Password') }}</label>

                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-12 col-form-label">{{ __('Confirm Password') }}</label>

                        <div class="col-md-12">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-5">
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    </div>
            </form>
        </div>    
    </div>
    
    <script type="text/javascript">
         $("#file").change(function(e) {
            var input = document.getElementById("file");
            var fReader = new FileReader();
            fReader.readAsDataURL(input.files[0]);
            fReader.onloadend = function(event){
                $("#file").parent().css( "background-image", "url("+event.target.result+")"  );
            }
        });
       
    </script>

    <style>
        #id{
            background-color: red;
        }
        #form_container{
        }
        #cover{
            background-image: url('{{asset("resources/adminCover.jpg")}}');
            background-color: #cccccc; /* Used if the image is unavailable */
            background-position: center; /* Center the image */
            background-repeat: no-repeat; /* Do not repeat the image */
            background-size: cover;
            border-radius: 8px;
            height: 400px;
        }
        .profile{
            margin-top: 100px;
            border-radius: 50%;
            width:280px;
            height:280px;
            background-color: hsla(0, 100%, 100%, 0.5);
            /* position: absolute; */
            padding: 15px;
            padding-top:15px;
            padding-bottom: 15px;
            
        }
        .profile1{
            height:250px;
            width:250px;
            border-radius: 50%;
            background-color: hsla(0, 100%, 100%, 0.8);
            padding:15px;
            padding-top:15px;
            padding-bottom: 15px;
        }
        .profile2{
            opacity:1;
            height:220px;
            width:220px;
            border-radius: 50%;
            background-image: url('{{asset("images_profile/" . auth()->user()->image . "")}}');
            background-color: #cccccc; /* Used if the image is unavailable */
            background-position: center; /* Center the image */
            background-repeat: no-repeat; /* Do not repeat the image */
            background-size: cover;
            border: 1px solid gray;
        }
        #file{
            width:220px;
            width:220px;
            cursor: pointer;
            opacity:0;
        }
        .fl{
            float: left;
        }
    </style>
@endsection