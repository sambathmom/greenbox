<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>QA</title>

    <link rel="icon" href="{{asset('resources/qa.svg')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('css/homestyle.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="http://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <a href="{{url('/')}}">
            <img src="{{asset('resources/qa.svg')}}" alt="logo" style="margin-left: 100px;width: 100px;height: 50px">
        </a>


        <div class="searchbar input-group col-md-5 col-sm-5">
            <form action="{{route('questoin.search.result')}}" method="get" class="form-inline my-2 my-lg-0" style="width: 100%">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" style="width: 80%" name="search">

                <button class=" btn btn-outline-secondary border-left-1 border my-2 my-sm-0" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>

        @guest
            <a href="{{route('register')}}"><button type="button" class="askbtn btn btn-primary">Ask Question</button><a>
                    @else
                        <a href="{{route('questions.create')}}"><button type="button" class="askbtn btn btn-primary">Ask Question</button><a>
                                @endguest

                                <ul class="mynav navbar-nav mr-auto">
                                    @guest
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                        </li>
                                    @else
                                        {{--<img class="ml-4" src="{{asset('resources/bell.svg')}}" style="width:35px;height:35px" alt="">--}}
                                        <a href="{{route('questions.index')}}">
                                            @if(auth()->user()->image != null || auth()->user()->image != '')
                                                <img class="ml-4" src="{{asset('images_profile/' . auth()->user()->image . '')}}" style="width:38px;height:38px;border-radius:50%;object-fit: cover"
                                                     alt="">
                                        </a>
                                        @else
                                            <img class="ml-4" src="{{asset('resources/profilepic.jpg')}}" style="width:38px;height:38px;border-radius:50%"
                                                 alt="">
                            </a>
                            @endif

                            <li class="nav-item dropdown ">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="{{route('questions.index')}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{route('profile.editprofile', auth()->user()->id)}}">
                                        <i class="fa fa-user-edit"></i>
                                        Edit Profile</a>
                                    {{--<a class="dropdown-item" href="#">Setting</a>--}}
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out-alt"></i>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endguest
                            </ul>
    </div>
</nav>
    <!-- End Navbar -->
    <!-- Start Profile -->
    <div class="row myprofilepage" style="background-image:  url({{asset('resources/wp.jpg')}}); height: 500px;  background-repeat: no-repeat;background-position: center; background-size: cover; opacity: 0.8 ;">
        <div class="col-md-6 pl-5">
            <div class="row c1 " style="position: relative; bottom: -250px">
                @if(auth()->user()->image != null || auth()->user()->image != '')
                    <img class=" ml-5" src="{{asset('images_profile/' . auth()->user()->image . '')}}" style="object-fit: cover;display:block;width:200px; height:200px ; border-radius:50%" alt="">
                    </a>
                @else 
                    <img class=" ml-5" src="{{asset('resources/profilepic.jpg')}}" style="display:block;width:200px; height:200px ;border-radius:50%" alt="">                    
                @endif
                <div class="c2 col-md-4 mr-5 text-white" style="position: relative; bottom: -160px">
                    <h3>{{auth()->user()->name}}</h3>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="card banpoint" style="position: relative; bottom: -300px">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 text-center" style="font-size:20px">
                                <p><i class="fas fa-circle text-success"></i><span class="ml-3">Answers</span></p>
                                <p class="ml-4">{{$countAnswers}}</p>
                        </div>

                        <div class="col-6 text-center" style="font-size:20px">
                                <p><i class="fas fa-circle text-primary"></i><span class="ml-3">Questions</span></p>
                            <p class="ml-4">{{$countQuestoins}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End Profile -->

    <div>
        @yield('content')
    </div>
   

    <!-- Start Footer -->

    
        <div class="col-12 mt-5">
            <div class="row bg-dark text-white pt-5 d-flex justify-content-center">
                <div class="col-md-2 mr-0 mt-sm-0 mt-5">

                    <img src="{{asset('resources/qa.svg')}}" style="width:100%" alt="image logo" class="navbar-brand mr-0 pr-0">

                </div>
                <div class="b col-md-2 ml-2">
                    <h3>PRODUCTS</h3>
                    <h5>Team</h5>
                    <h5>Talent</h5>
                    <h5>Enterprise</h5>
                    <h5>Engagement</h5>
                </div>
                <div class="b col-md-3 ml-2">
                    <h3>COMMAPNY</h3>
                    <h5>About</h5>
                    <h5>Press</h5>
                    <h5>Work Here</h5>
                    <h5>Legal</h5>
                    <h5>Privacy Policy</h5>
                    <h5>Contact Us</h5>
                </div>
                <div class="b col-md-4 ml-2">
                    <h3>FRONTROOM COMMUNITY NETWORK</h3>
                    <div class="pl-4">
                        <i class="fab fa-facebook-square mr-3" style="font-size:50px"></i>
                        <i class="fab fa-blogger  mr-3" style="font-size:50px"></i>
                        <i class="fab fa-github  mr-3" style="font-size:50px"></i>
                        <i class="fas fa-globe  mr-3" style="font-size:50px"></i>
                        <i class="fab fa-youtube  mr-3" style="font-size:50px"></i>
                    </div>
                    <p class="pl-4 mt-4">QA is the first website in cambodia.that create to help developer to find the best solution for them for free.and it is very easy to use and have a lot of questions ,andwers</p>

                </div>
            </div>
        </div>
    
    <!-- End Footer -->
</body>

</html>