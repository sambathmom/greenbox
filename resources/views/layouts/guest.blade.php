<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Q&A</title>

    <link rel="icon" href="{{asset('a.svg')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('/css/homestyle.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <a href="#">
                <img src="../Resources/box.svg" alt="logo" style="margin-left: 100px;">
            </a>


            <div class="searchbar input-group col-md-4 ">
                <input class="form-control py-2 border-right-0 border" type="search" placeholder="search" id="example-search-input">
                <span class="input-group-append">
                    <button class=" btn btn-outline-secondary border-left-0 border" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>

            @guest
                <a href="{{route('register')}}"><button type="button" class="askbtn btn btn-primary">Ask Question</button><a>
            @else
                <a href="{{route('questions.create')}}"><button type="button" class="askbtn btn btn-primary">Ask Question</button><a>                
            @endguest

            <ul class="mynav navbar-nav mr-auto">

                @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/index') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
                @endif
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="row mt-5">
        @yield('content')
    </div>

    <div class="row mt-5">

        <!-- Start Footer -->


        <div class="col-12 mt-5">
            <div class="row bg-dark text-white pt-5 d-flex justify-content-center">
                <div class="col-md-2 mr-0 mt-sm-0 mt-5">

                    <img src="../Resources/box.svg" style="width:100%" alt="image logo" class="navbar-brand mr-0 pr-0">

                </div>
                <div class="b col-md-2 ml-2">
                    <h3>PRODUCTS</h3>
                    <h5>Team</h5>
                    <h5>Talent</h5>
                    <h5>Enterprise</h5>
                    <h5>Engagement</h5>
                </div>
                <div class="b col-md-3 ml-2">
                    <h3>COMMAPNY</h3>
                    <h5>About</h5>
                    <h5>Press</h5>
                    <h5>Work Here</h5>
                    <h5>Legal</h5>
                    <h5>Privacy Policy</h5>
                    <h5>Contact Us</h5>
                </div>
                <div class="b col-md-4 ml-2">
                    <h3>FRONTROOM COMMUNITY NETWORK</h3>
                    <div class="pl-4">
                        <i class="fab fa-facebook-square mr-3" style="font-size:50px"></i>
                        <i class="fab fa-blogger  mr-3" style="font-size:50px"></i>
                        <i class="fab fa-github  mr-3" style="font-size:50px"></i>
                        <i class="fas fa-globe  mr-3" style="font-size:50px"></i>
                        <i class="fab fa-youtube  mr-3" style="font-size:50px"></i>
                    </div>
                    <p class="pl-4 mt-4">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perferendis
                        dicta porro nostrum fuga similique debitis neque consectetur vitae, placeat, a incidunt.
                        Quisquam, quae. Quos, sint! Nemo, fugit corrupti? Reiciendis, laborum!</p>

                </div>
            </div>
        </div>

        <!-- End Footer -->
    </div>
</body>

</html>