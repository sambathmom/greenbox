@foreach($categories as $category)
<div class="row mt-2">
    <div class="col-md-1">
        <img src="../Resources/javaIcon.jpg" class="ml-3" style=" width: 30px;" alt="">
    </div>
    <div class="col-md-9">
        <p class="ml-4">Java </p>
    </div>
</div>
@endforeach
<div class="row mt-2">
    <div class="col-md-1">
        <img src="../Resources/csharpIcon.png" class="ml-3" style=" width: 30px;" alt="">
    </div>
    <div class="col-md-9">
        <p class="ml-4">C# </p>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-1">
        <img src="../Resources/c-plus-plus-logo.png" class="ml-3" style=" width: 30px;" alt="">
    </div>
    <div class="col-md-9">
        <p class="ml-4">C++ </p>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-1">
        <img src="../Resources/html.png" class="ml-3" style=" width: 30px;" alt="">
    </div>
    <div class="col-md-9">
        <p class="ml-4">Html</p>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-1">
        <img src="../Resources/javaScriptIcon.png" class="ml-3" style=" width: 30px;" alt="">
    </div>
    <div class="col-md-9">
        <p class="ml-4">JavaScript </p>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-1">
        <img src="../Resources/php.png" class="ml-3" style=" width: 30px;" alt="">
    </div>
    <div class="col-md-9">
        <p class="ml-4">PHP </p>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-1">
        <img src="../Resources/pythonIcon.png" class="ml-3" style=" width: 25px;" alt="">
    </div>
    <div class="col-md-9">
        <p class="ml-4">Python </p>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-1">
        <img src="../Resources/images.png" class="ml-3" style=" width: 25px;" alt="">
    </div>
    <div class="col-md-9">
        <p class="ml-4">Swift </p>
    </div>
</div>
<div class="row mt-2">
    <div class="col-md-1">
        <img src="../Resources/rubyIcon.png" class="ml-3" style=" width: 25px;" alt="">
    </div>
    <div class="col-md-9">
        <p class="ml-4">Ruby </p>
    </div>
</div>