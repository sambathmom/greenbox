@extends('layouts.user2')


@section('content')

<div class="row mt-5 ml-md-5">
    <div class="col-md-2 col-12">&nbsp;</div>
    <div class="row mt-5">
            {{-- <div class="col-md-1 col-12"></div> --}}
            <div class="col-md-10 col-12 ml-3  ml-md-0">
        
                <!-- Start Question -->
                <div>
                    <h2>Question</h2>
                    <div class="row">
                        <div class="col-sm-1 text-center">
                        </div>
                        <div class="col-sm-12">
                            <!-- start Card -->
                            <div class="mycard card mt-5" style="width: 100%">
                                <div class="card-body">
                                    <div class="float-right border bg-light rounded" style="position: absolute; top:-18px;border-radius: 8px !important; ">
                                        <img src="{{asset('img_upload/' . $question->category->icon . '')}}" class="mr-0  col-12" style="width: 35px;padding-left: 0px;padding-right: 0px;"
                                            alt="">
                                        <label for="" class="pr-2 ">{{$question->category->name}}</label>
                                    </div>
                                    <div class="row">
                                        <div class="mt-4 userArea col-2">
                                                @if($question->user->image != null || $question->user->image != "")
                                                <img class="qprofile" src="{{asset('img_upload/' . $question->user->image . '')}}" alt="">
                                                @else
                                                <img class="qprofile" src="{{asset('resources/profilepic.jpg')}}" alt="">
                                                
                                                @endif
                                            <p class="qname text-xs-center">{{$question->user->name}}</p>
                                        </div>
        
                                        <div class="col-sm-12 col-md-10 col-12">
                                            <div>
                                                <h5 class="card-title">{{$question->title}}</h5>
                                                <div class="">
                                                    <img class="test" src="{{asset('resources/date.svg')}}" style="width:15px;height:15px;"
                                                        alt="">
                                                    <h6 class="ml-4 card-subtitle mb-2 text-muted">{{$question->updated_at}}</h6>
                                                </div>
        
                                                <p class="card-text">{!! $question->description !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end Card -->
                        </div>
                    </div>
                {{-- </div>  --}}
                <!-- End Question -->
        
                <!-- Start Answer -->
                <div>
                <h2 class="border-top mt-5 mb-2">Answer</h2>
                    
                    <div class="row">
                        <div class="col-sm-1 text-center">
                            <p class="ml-10 mt-5">Liked</p>
                            <p class="ml-10" style="font-size:12px">12 Likes</p>
                        </div>
                        <div class="col-sm-11">
                            <!-- start Card -->
                            <div class="mycard card mt-5" style="width: 100%">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="mt-4 userArea col-2">
                                                @if($question->user->image != null || $question->user->image != "")
                                                <img class="qprofile" src="{{asset('img_upload/' . $question->user->image . '')}}" alt="">
                                                @else
                                                <img class="qprofile" src="{{asset('resources/profilepic.jpg')}}" alt="">
                                                
                                                @endif
                                            <p class="qname text-xs-center">{{$answer->user->name}}</p>
                                        </div>
        
                                        <div class="col-sm-12 col-md-10 col-12">
        
                                            <div>
                                                <div class="">
                                                    <img class="test" src="{{asset('resources/date.svg')}}" style="width:15px;height:15px;"
                                                        alt="">
                                                    <h6 class="ml-4 card-subtitle mb-2 text-muted">{{$answer->updated_at}}</h6>
                                                </div>
        
                                                <p class="card-text">{!! $answer->answer !!}</p>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="card bg-light">
                                                    {{-- <div class="card-body">
                                                
                                                    </div> --}}
                                                </div>       
                                            </div>

                                            <div class="d-flex mt-3">
                                                <div>
                                                     <div class="row col-12">
                                                        <a href="{{route('answers.edit', $answer->id)}}">
                                                            <button type="button" class="btn btn-info" style="height:40px">
                                                                <img class="mb-2 " src="{{asset('resources/edit.png')}}" style="height:20px;"
                                                                            alt="">
                                                                            Edit&nbsp;
                                                            </button>
                                                         </a>
                                                     </div>
                                                 </div>
                                                 <div class="col-md-2">
                                                     <div class="row col-12">
                                                         <div class="row col-12">
                                                             <button type="button" class="btn btn-danger delete-answer" data-id="{{$answer->id}}" data-toggle="modal">
                                                                     <img class="mb-2 " src="{{asset('resources/delete.png')}}" style="height:18px;"
                                                                     alt="">
                                                                     Delete
                                                             </button>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end Card -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Answer -->
            </div>
        </div>
</div>

<!-- The Modal -->
<div class="modal" id="deleteAnswerModal">
    <div class="modal-dialog">
        <form method="post" action="{{route('answers.destroy')}}">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Comfirmation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            
                <!-- Modal body -->
                <div class="modal-body">
                    @csrf
                    <input type="hidden" id="deleteAnswer" name="id">
                    Are you sure want to delete this answer?
                </div>
            
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Delete"/>                    
                </div>
        
            </div>
        </form>
    </div>
</div>
    
<script>
    $(document).ready(function() {

        $('.delete-answer').on('click', function() {

            $questionId = $(this).data('id');
            $('#deleteAnswer').val($questionId);
            $('#deleteAnswerModal').modal('show');
        });
        
    });

</script>
@endsection
