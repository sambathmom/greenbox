@extends('layouts.user')

@section('content')

    <div class="col-md-1 col-12">&nbsp;</div>
    <div class="col-md-10 col-12 ml-3  ml-md-0">
        <!-- Start filter -->
    <div class="row offset-md-2 mt-5">
        <div class="dropdown pl-2">
            <h4> Edit answer</h4>
        </div>
    </div>

    <!-- End filter -->

    <!-- Start Content -->
    <div class="row mt-2">
        <!-- Start collumn1 -->
        <div class="col-md-10 offset-md-1">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <!-- Start card -->
                    <div class="card col-12 mt-4 mb-3">
                        <div class="card-body mt-0 pt-3">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <form method="POST" action="{{route('answers.update')}} " >
                                        @csrf

                                        <input name="id" value="{{$answer->id}}" type="hidden"/>
                                        <input value="{{$answer->question->id}}" name="question_id" type="hidden"/>

                                        <label for="title">Answer</label>
                                        <div class="input-group mb-3">
                                        <textarea class="form-control" name="answer" id="textEdit">{{$answer->answer}}</textarea>
                                        </div>
                                        <div class="input-group">
                                            <a href="{{route('answers.question.detail', $answer->question->id)}}"></a>
                                            <button class="btn btn-outline-primary my-2 my-sm-0 col-2 offset-7" type="submit">
                                                Cancel
                                            </button>
                                            <button class="btn btn-outline-success bg-primary text-white my-2 my-sm-0 col-2 ml-2" type="submit">
                                                Submit
                                            </button>

                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- End card -->
                </div>
            </div>
        </div>
        <!-- End collumn1 -->
    </div>
    <!-- End Content-->

    <script type="application/x-javascript">   
        tinymce.init({selector:'#textEdit'});
    </script>

@endsection