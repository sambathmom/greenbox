@extends('layouts.user')


@section('content')
<div class="col-md-1 col-12">&nbsp;</div>
    <div class="col-md-7 col-12 ml-3  ml-md-0">
        @if($questions->count() == 0)
        <div>
            <h3>No questions related to this category.</h3>
        </div>
        @endif

        @foreach($questions as $question)
        <div>
            <div class="mycard card mt-5" style="width: 100%">
                <div class="card-body">
                    <div class="float-right border bg-light rounded" style="position: absolute; top:-18px;border-radius: 8px !important; ">
                        <img src="{{asset('img_upload/' . $question->category->icon . '')}}" class="mr-0  col-12" style="width: 35px;padding-left: 0px;padding-right: 0px;"
                            alt="">
                        <label for="" class="pr-2 ">{{$question->category->name}}</label>
                    </div>
                    <div class="row">
                        <div class="mt-4 userArea col-2 text-center">
                            @if($question->user->image != null || $question->user->image != '')                                
                                <img class="qprofile" src="{{asset('images_profile/' . $question->user->image . '')}}" alt="">
                            @else 
                                <img class="qprofile" src="{{asset('resources/profilepic.jpg')}}" alt="">
                            @endif
                            <p class="qname text-xs-center">{{$question->user->name}}</p>
                        </div>

                        <div class="col-sm-10 col-md-10 col-12">

                            <div class="mylink">
                                <a class="" href="{{route('welcome.questions.detail', $question->id)}}">
                                    <h5 class="card-title">{{$question->title}}</h5></a>
                                <div class="">
                                    <img class="test" src="../Resources/date.svg" style="width:15px;height:15px;"
                                        alt="">
                                    <h6 class="ml-4 card-subtitle mb-2 text-muted">{{$question->updated_at}}</h6>
                                </div>

                                <p class="card-text">{!!$question->description!!}</p>
                            </div>
                            <div class="d-flex justify-content-around mt-2">
                                <span><img src="{{asset('resources/answer.svg')}}" style="width:25px;height:25px" alt="">{{$question->answers()->count()}}
                                    answers</span>
                                <span><img src="{{asset('resources/comment.svg')}}" style="width:25px;height:25px" alt="">{{$question->answers()->count()}}
                                    comments</span>
                                <span><img src="{{asset('resources/vote.svg')}}" style="width:25px;height:25px" alt="">38
                                    likes</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Card -->
        </div>
        @endforeach

        @if(\Request::is('questions/search/result'))
        @else
            <div style="margin-top:15px">
                <?php echo $questions->links(); ?>        
            </div>
        @endif
    </div>

    <!-- Start Tage -->
    <div class="col-md-3 col-12 mylink">
            <div class="border-bottom">
                <img src="{{asset('resources/tag.png')}}" class="col-12 mb-2" style=" width: 65px;" alt="">
                <label for="" class="pr-2 ">Tags</label>
            </div>
            @foreach($categories as $category)
                <a href="{{route('questions.type', $category->id)}}">
                    <div class="row mt-2 ml-1">
                        <div class="">
                            <img src="{{asset('img_upload/' . $category->icon . '')}}" class="ml-3" style=" width: 30px;height:30px;object-fit:cover" alt="">
                        </div>
                        <div class="">
                            <p class="ml-4">{{$category->name}}</p>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
</div>    
<!-- End Tage -->

@endsection
