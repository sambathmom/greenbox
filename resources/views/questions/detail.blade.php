@extends('layouts.user')

@section('content')
    <div class="row mt-5">
        <div class="col-md-1 col-12"></div>
        <div class="col-md-7 col-12 ml-3  ml-md-0">
            <!-- Start Question -->
            <div>
                <h2>Question</h2>
                <div class="row">

                    <div class="col-sm-11">
                        <!-- start Card -->
                        <div class="mycard card mt-5" style="width: 100%">
                            <div class="card-body">
                                <div class="float-right border bg-light rounded" style="position: absolute; top:-18px;border-radius: 8px !important; ">
                                    <img src="{{asset('img_upload/' . $question->category->icon . '')}}" class="mr-0 ml-1 col-12" style="width: 35px;padding-left: 0px;padding-right: 0px;"
                                         alt="">
                                    <label for="" class="pr-2 ">{{$question->category->name}}</label>
                                </div>
                                <div class="row">
                                    {{--<div class="mt-4 userArea col-2 text-center">--}}
                                        {{--<img class="qprofile" src="{{asset('images_profile/' . $question->user->image . '')}}" alt="">--}}
                                        {{--<p class="qname text-xs-center">{{$question->user->name}}</p>--}}
                                    {{--</div>--}}
                                <div class="col-sm-12 col-md-10 col-12">
                                    <div>
                                        <h5 class="card-title">{{$question->title}} here is an error within one of your views. 
                                            If there is a more detailed stack trace it should show you details of a view,</h5>
                                        <div class="">
                                            <img class="test" src="{{asset('resources/date.svg')}}" style="width:15px;height:15px;"
                                                alt="">
                                            <h6 class="ml-4 card-subtitle mb-2 text-muted">{{$question->updated_at}}</h6>
                                        </div>
                                        @if($question->image != "" || $question->image != null)
                                        <img src="{{asset('img_upload/' . $question->image )}}"  width="500px" />
                                        @endif
                                        <p class="card-text">{!!$question->description!!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end Card -->
                    </div>
                </div>
            </div>
        </div> 
   
        <!-- End Question -->
        
        <!-- Start Answer -->
        @foreach($question->answers as $answer)
        <div>
            <h2 class="border-top mt-5 mb-2">Answer</h2>
            <div class="row" style="margin-left:-90px !important">
                <div class="col-sm-1 text-center">
                    @guest
                        <p class="ml-10 mt-5">Liked</p>
                    @else
                        <?php $thisPerson = false ?>
                        @foreach($answer->likes as $like)
                            @if($like->user_id == auth()->user()->id)
                                <?php $thisPerson = true ?>
                            @endif
                        @endforeach
                        @if($thisPerson)
                        <p class="ml-10 mt-5">Like</p>
                        @else
                            <img class="ml-10 mt-5 like" width="50px" src="{{asset('resources/icons8-elections_filled.png')}}" 
                            data-answer-id={{$answer->id}}>
                        @endif
                    @endguest
                    
                    <p class="ml-10" style="font-size:12px">{{$answer->likes->count()}} Likes</p>
                </div>
                <div class="col-sm-10">
                    <!-- start Card -->
                    <div class="mycard card mt-5" style="width: 100%">
                        <div class="card-body">
                            <div class="row">
                                <div class="mt-4 userArea col-2 text-center">
                                    @if($answer->user->image != null || $answer->user->image != "")                                        
                                    <img class="qprofile" src="{{asset('images_profile/' . $answer->user->image .'')}}" alt="">
                                    @else
                                    <img class="qprofile" src="{{asset('resources/profilepic.jpg')}}" alt="">                                    
                                    @endif
                    
                                    <p class="qname text-xs-center">{{$answer->user->name}}</p>
                                </div>
                                <div class="col-sm-12 col-md-10 col-12">
                                    <div>
                                        <div class="">
                                            <img class="test" src="{{asset('resources/date.svg')}}" style="width:15px;height:15px;"
                                                alt="">
                                            <h6 class="ml-4 card-subtitle mb-2 text-muted">{{$answer->updated_at}}</h6>
                                        </div>
                                        <p class="card-text">{!!$answer->answer!!}</p>
                                    </div>
                                                
                                    <div class="col-10 offset-1 mt-4">
                                        @foreach($answer->comments as $comment)
                                            <div class="row">
                                                <div class="col-md-2 text-center">
                                                    @if($comment->user->image != null || $comment->user->image != "")
                                                    <img src="{{asset('images_profile/' . $comment->user->image .'')}}" class="qprofileComment"
                                                        style="border-radius: 50%" alt="">
                                                    @else 
                                                    <img src="{{asset('resources/profilepic.jpg')}}" class="qprofileComment"
                                                    style="border-radius: 50%" alt="">
                                                    @endif
                                                    <label for="" class="mt-2">{{$comment->user->name}}</label>
                                                </div>
                                                <div class="col-md-10">
                                                <p>{!!$comment->comment!!}<small class="text-info">{{$comment->update_at}}</small> </p>          
                                                </div>
                                            </div>
                                        @endforeach

                                        @guest
                                        @else
                                        <div class="row border-top">
                                            <form method="POST" action="{{route('comments.store')}}">
                                                @csrf
                                                <input type="hidden" value="{{$question->id}}" name="question_id"/>
                                                <input type="hidden" value="{{$answer->id}}" name="answer_id" />
                                                <label for="title">Comment</label>
                                                <div class="input-group mb-3">
                                                    <textarea class="form-control text-comment" name="comment"></textarea>
                                                </div>
                                
                                                <div class="input-group">
                                                    <button class="btn btn-outline-success bg-primary text-white my-2 my-sm-0 col-2 ml-2" type="submit">
                                                        Submit
                                                    </button>
                                
                                                </div>
                                            </form>                                           
                                        </div>
                                        @endguest
                                    </div>
                                    </div>
                                </div>
                                <!-- end Card -->
                            </div>

                            </div>
                        </div>
                    </div>
                    <!-- End Answer -->
                </div>
            
        <!-- End Answer -->
        @endforeach

        @guest
            @else
            <div class="row pl-3 pt-5">
                <form method="post" action="{{route('answers.store')}}">
                    @csrf
                    <input type="hidden" value="{{$question->id}}" name="question_id"/>

                    <label for="title">Answer</label>
                    <div class="input-group mb-3">
                        <textarea class="form-control" name="answer" id="textAnswer"></textarea>
                        @if ($errors->has('answer'))
                            <span class="error">{{ $errors->first('answer') }}</span>
                        @endif
                    </div>

                    <div class="input-group">
                        <button class="btn btn-outline-success bg-primary text-white my-2 my-sm-0 col-2 ml-2" type="submit">
                            Submit
                        </button>

                    </div>
                </form>
            </div>
        @endguest
        
    </div>

        <div class="col-md-3 col-12 mylink">
            <div class="border-bottom">
                <img src="{{asset('resources/tag.png')}}" class="col-12 mb-2" style=" width: 65px;" alt="">
                <label for="" class="pr-2 ">Tags</label>
            </div>
            @foreach($categories as $category)
                <a href="{{route('questions.type', $category->id)}}">
                    <div class="row mt-2 ml-1">
                        <div class="">
                            <img src="{{asset('img_upload/' . $category->icon . '')}}" class="ml-3" style=" width: 30px;height:30px;object-fit:cover" alt="">
                        </div>
                        <div class="">
                            <p class="ml-4">{{$category->name}}</p>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    

    <script type="application/x-javascript">
        tinymce.init({selector:'.text-comment'});
        tinymce.init({selector:'#textAnswer'});

        $(document).ready(function () {

            $('.like').on('click', function() {
                var answerId = $(this).data('answer-id');

                $.ajax({
                    url: "{{route('like.store')}}",
                    type: "post",
                    data: {"_token": "{{ csrf_token() }}", "answer_id": answerId},
                    success: function(html){
                        location.reload();
                    }
                });

            });
        });

    </script>
@endsection