@extends('layouts.user2')


@section('content')

 <!-- Start question -->
 <div class="row mt-md-5 mt-sm-0">
    <div class="col-md-2 col-12">&nbsp;</div>
    <div class="col-md-8 col-12 ml-3 mt-5 ml-md-0">

        <!-- Start Dropdown -->
        <div class="dropdown show">
            <a class="btn btn-secondary dropdown-toggle dp" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Asked Questions
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="{{route('questions.index')}}">Asked Questions</a>
                <a class="dropdown-item" href="{{route('questions.answers.index')}}">Answered Questions</a>
            </div>
        </div>
        <!-- Start Dropdown -->
        @if ($questions->count() == 0)
            <div style="margin-top: 10px">
                @if(\Request::is('profile/questions/index'))
                    <h3>You haven't asked any questions yet.</h3>
                @else
                    <h3>There is no any questions macth your search " {{$searchStr}} ".</h3>            
                @endif
            </div>
        @endif

        @foreach($questions as $question)
        <div>
            <div class="mycard card mt-5" style="width: 100%">
                <div class="card-body">
                    <div class="float-right border bg-light rounded" style="position: absolute; top:-18px;border-radius: 8px !important; ">
                        <img src="{{asset('img_upload/' . $question->category->icon . '')}}" class="mr-0  col-12" style="width: 35px;padding-left: 0px;padding-right: 0px;"
                            alt="">
                        <label for="" class="pr-2 ">{{$question->category->name}}</label>
                    </div>
                    <div class="row">

                        <div class="ml-5 mt-md-4 col-sm-10 col-md-10 col-12">

                            <div>
                                <h5 class="card-title">{{$question->title}}</h5>
                                <div class="">
                                    <img class="test" src="{{asset('resources/date.svg')}}" style="width:15px;height:15px;"
                                        alt="">
                                    <h6 class="ml-4 card-subtitle mb-2 text-muted">{{$question->updated_at}}</h6>
                                </div>

                                <p class="card-text">{!!$question->description!!}</p>
                            </div>
                            <div class="d-flex mt-3">
                               <div>
                                    <div class="row col-12">
                                        <a href="{{route('questions.edit', $question->id)}}">
                                        <button type="button" class="btn btn-info" style="height:40px">
                                            <img class="mb-2 " src="{{asset('resources/edit.png')}}" style="height:20px;"
                                                        alt="">
                                                        Edit&nbsp;
                                        </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="row col-12">
                                        <div class="row col-12">
                                            <button type="button" class="btn btn-danger delete-question" data-id="{{$question->id}}" data-toggle="modal">
                                                    <img class="mb-2 " src="{{asset('resources/delete.png')}}" style="height:18px;"
                                                    alt="">
                                                    Delete
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Card -->
        </div>
        @endforeach

        @if(\Request::is('profile/questions/search'))
        @else
            <div style="margin-top:15px">
                <?php echo $questions->links(); ?>        
            </div>
        @endif
    </div>
</div>
<!-- End question -->
      
<!-- The Modal -->
<div class="modal" id="deleteQuestionModal">
    <div class="modal-dialog">
        <form method="post" action="{{route('questions.destroy')}}">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Comfirmation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            
                <!-- Modal body -->
                <div class="modal-body">
                    @csrf
                    <input type="hidden" id="deleteQuestion" name="id">
                    Are you sure want to delete this question?
                </div>
            
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Delete"/>                    
                </div>
        
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('.delete-question').on('click', function() {

            $questionId = $(this).data('id');
            $('#deleteQuestion').val($questionId);
            $('#deleteQuestionModal').modal('show');
        });
        
    });

</script>
@endsection
