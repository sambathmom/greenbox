@extends('layouts.user2')


@section('content')

<div class="row mt-5">
    <div class="col-md-2 col-12">&nbsp;</div>
    <div class="col-md-8 col-12 ml-3  ml-md-0">

        <!-- Start Dropdown -->
        <div class="dropdown show">
            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Answered Questions
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="{{route('questions.index')}}">Asked Questions</a>
                <a class="dropdown-item" href="{{route('questions.answers.index')}}">Answered Questions</a>
            </div>
        </div>

        @if ($questions->count() == 0)
            <div style="margin-top: 10px">
                @if(\Request::is('profile/questions/answers/index'))
                    <h3>You haven't answered any questoins yet.</h3>
                @else
                    <h3>There is no any questions macth your search " {{$searchStr}} ".</h3>            
                @endif
            </div>
        @endif
        <!-- End Dropdown -->

        <!--Start questoins answered -->
        @foreach($questions as $question)
        <div>
            <div class="mycard card mt-5" style="width: 100%">
                <div class="card-body">
                    <div class="float-right border bg-light rounded" style="position: absolute; top:-18px;border-radius: 8px !important; ">
                        <img src="{{asset('img_upload/' . $question->category->icon . '')}}" class="mr-0  col-12" style="width: 35px;padding-left: 0px;padding-right: 0px;"
                            alt="">
                        <label for="" class="pr-2 ">{{$question->category->name}}</label>
                    </div>
                    <div class="row">
                        {{--<div class="mt-4 userArea col-2">--}}
                            {{--@if($question->user->image != null || $question->user->image != "")--}}
                            {{--<img class="qprofile" src="{{asset('img_upload/' . $question->user->image . '')}}" alt="">--}}
                            {{--@else--}}
                            {{--<img class="qprofile" src="{{asset('resources/profilepic.jpg')}}" alt="">--}}
                            {{----}}
                            {{--@endif--}}
                            {{--<p class="qname text-xs-center">{{auth()->user()->name}}</p>--}}
                        {{--</div>--}}

                        <div class="col-sm-10 ml-md-5 my-md-5 col-md-10 col-12">

                            <div>
                                <h5 class="card-title">{{$question->title}}</h5>
                                <div class="">
                                    <img class="test" src="{{asset('resources/date.svg')}}" style="width:15px;height:15px;"
                                        alt="">
                                    <h6 class="ml-4 card-subtitle mb-2 text-muted">{{$question->updated_at}}</h6>
                                </div>

                                <p class="card-text">{!!$question->description!!}</p>
                            </div>
                            <div class="d-flex mt-3">
                                <div>
                                    <div class="row col-12">
                                        <a href="{{route('answers.question.detail', $question->id)}}">
                                            <button type="button" class="btn btn-success" style="height:40px">
                                                <img src="{{asset('resources/answer.png')}}" style="height:17px;"
                                                            alt="">
                                                            My Answer&nbsp;
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end Card --> 
        </div>
        @endforeach
        <!--End questoins answered -->

        @if(\Request::is('profile/questions/answers/index/search'))
        @else
            <div style="margin-top:15px">
                <?php echo $questions->links(); ?>        
            </div>
        @endif
    </div>
</div>
@endsection
