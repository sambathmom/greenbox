@extends('layouts.user')

@section('content')

    <div class="col-md-1 col-12">&nbsp;</div>
    <div class="col-md-10 col-12 ml-3  ml-md-0">
        <!-- Start filter -->
    <div class="row offset-md-2 mt-5">
        <div class="dropdown pl-2">
            <h4> Make a Question</h4>
        </div>
    </div>

    <!-- End filter -->

    <!-- Start Content -->
    <div class="row mt-2">
        <!-- Start collumn1 -->
        <div class="col-md-10 offset-md-1">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <!-- Start card -->
                    <div class="card col-12 mt-4 mb-3">
                        <div class="card-body mt-0 pt-3">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <form method="POST" action="{{route('questions.store')}} " enctype="multipart/form-data">
                                        @csrf
                                        <label for="title">Title</label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" name="title" aria-label="Text input with checkbox">
                                            @if ($errors->has('title'))
                                                <span class="error">{{ $errors->first('title') }}</span>
                                            @endif
                                        </div>

                                        <label for="title">Tag</label>
                                        <div class="dropdown show mb-3">
                
                                            <select name="cate_id" class="form-control"/>
                                                @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <label for="title">Image</label>
                                        <div class="input-group mb-3">
                                            <input type="file" name="image" class="form-control" />
                                        </div>

                                        <label for="title">Desciption</label>
                                        <div class="input-group mb-3">
                                            <textarea rows="20" class="form-control" name="description"  aria-label="Text input with checkbox " id="textEdit"></textarea>
                                        </div>
                                        <div class="input-group">
                                            <button class="btn btn-outline-primary my-2 my-sm-0 col-2 offset-7" type="submit">
                                                Cancel
                                            </button>
                                            <button class="btn btn-outline-success bg-primary text-white my-2 my-sm-0 col-2 ml-2" type="submit">
                                                Submit
                                            </button>

                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- End card -->
                </div>
            </div>
        </div>
        <!-- End collumn1 -->
    </div>
    <!-- End Content-->

    <script type="application/x-javascript">   
        tinymce.init({selector:'#textEdit'});
    </script>

@endsection