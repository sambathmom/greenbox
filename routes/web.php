<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'WelcomeController@welcome');
Route::get('/welcome/questions/detail/{questionId}', ['as'=>'welcome.questions.detail','uses'=>'WelcomeController@questionDetail']);
Route::get('admins/login', ['as'=>'admins.login','uses'=>'Auth\LoginController@showLoginForm']);
Route::get('questions/type/{categoryId}', ['as'=>'questions.type','uses'=>'WelcomeController@getQuestionsByCategoryId']);Route::get('admins/login', ['as'=>'admins.login','uses'=>'Auth\LoginController@showLoginForm']);
Route::get('/questions/search/result', ['as'=>'questoin.search.result','uses'=>'WelcomeController@getQuestoinSearch']);        

Auth::routes();

Route::group(['middleware' => ['App\Http\Middleware\AdminMiddleware', 'auth']], function()
{
    /* Admin page */
    Route::get('/admins/index', ['as'=>'admins.index','uses'=>'Admins\AdminController@index']);
    Route::get('/admins/categories/index', ['as'=>'admins.categories.index','uses'=>'CategoriesController@index']);
    Route::get('/admins/categories/create', ['as'=>'admins.categories.create','uses'=>'CategoriesController@create']);
    Route::post('/admins/categories/store', ['as'=>'admins.categories.store','uses'=>'CategoriesController@store']);
    Route::get('/admins/categories/edit/{categoryId}', ['as'=>'admins.categories.edit','uses'=>'CategoriesController@edit']);
    Route::post('/admins/categories/update', ['as'=>'admins.categories.update','uses'=>'CategoriesController@update']);
    Route::post('/admins/categories/destroy', ['as'=>'admins.categories.destroy','uses'=>'CategoriesController@destroy']);
    Route::get('/admins/users/index', ['as'=>'admins.users.index','uses'=>'Admins\AdminController@getUsers']);    

});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile/questions/index', ['as'=>'questions.index','uses'=>'QuestionsController@index']);
    
    /* Route qustions */
    Route::get('/profile/questions/create', ['as'=>'questions.create','uses'=>'QuestionsController@create']);
    Route::post('/profile/questions/store', ['as'=>'questions.store','uses'=>'QuestionsController@store']);
    Route::get('/questions/detail/{questionId}', ['as'=>'questions.detail','uses'=>'QuestionsController@detail']);
    Route::get('/profile/questions/edit/{questionId}', ['as'=>'questions.edit','uses'=>'QuestionsController@edit']);
    Route::post('/profile/questions/update', ['as'=>'questions.update','uses'=>'QuestionsController@update']);
    Route::post('/profile/questions/destroy', ['as'=>'questions.destroy','uses'=>'QuestionsController@destroy']);
    Route::get('/profile/questions/answers/index', ['as'=>'questions.answers.index','uses'=>'QuestionsController@qustoinsAnswered']);
    
    /*Route answers */
    Route::get('/profile/answers/detail/{questionsId}', ['as'=>'answers.question.detail','uses'=>'AnswersController@answerQuestionDetail']);
    Route::get('/answers/question/{questionsId}', ['as'=>'answers.question.create','uses'=>'AnswersController@create']);
    Route::post('/answers/store', ['as'=>'answers.store','uses'=>'AnswersController@store']);
    Route::get('/profile/answers/edit/{answerId}', ['as'=>'answers.edit','uses'=>'AnswersController@edit']);
    Route::post('/profile/answers/update', ['as'=>'answers.update','uses'=>'AnswersController@update']);
    Route::post('/profile/answers/destroy', ['as'=>'answers.destroy','uses'=>'AnswersController@destroy']);
    
    /* Route comments */
    Route::post('/comments/store', ['as'=>'comments.store','uses'=>'CommentsController@store']);
    
    
    Route::get('/profile/edit/{userId}', ['as'=>'profile.editprofile','uses'=>'UserController@editProfile']);
    Route::post('/profile/update', ['as'=>'profile.update','uses'=>'UserController@update']);    


    //Search
    Route::get('/profile/questions/search', ['as'=>'profile.questoin.search.result','uses'=>'QuestionsController@getQuestionSearch']);    
    Route::get('profile/questions/answers/index/search', ['as'=>'profile.answered.questoin.search.result','uses'=>'QuestionsController@getAnswerQuestoinsSearch']);    
    Route::get('profile/answers/detail/search', ['as'=>'profile.answered.questoin.detail.search.result','uses'=>'QuestionsController@update']);        
   
    Route::post('/like/store', ['as'=>'like.store','uses'=>'LikesController@store']);        
    
});

